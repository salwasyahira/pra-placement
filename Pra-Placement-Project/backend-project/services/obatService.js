const { response, request } = require("express");
const { errorMonitor } = require("nodemailer/lib/xoauth2");

module.exports = exports = (app, pool) => {
  app.get("/api/obat", (request, response) => {
    const query = `SELECT * FROM obat WHERE is_delete = false`;

    pool.query(query, (error, result) => {
      if (error) {
        return response.send(400, {
          success: false,
          data: error,
        });
      } else {
        return response.send(200, {
          success: true,
          data: result.rows,
        });
      }
    });
  });

  app.get("/api/obat/:id", (request, response) => {
    const { id } = request.params;
    const query = `SELECT * FROM obat WHERE id = ${id}`;

    pool.query(query, (error, result) => {
      // console.log(result)
      if (error) {
        return response.send(400, {
          success: false,
          data: error,
        });
      } else {
        return response.send(200, {
          success: true,
          data: result.rows[0], // ada di indeks ke 0
        });
      }
    });
  });

  app.get("/api/search/obat", (request, response) => {
    var { name } = request.query;

    const queryAll = `SELECT * FROM obat WHERE is_delete = false`;
    const query = `SELECT * FROM obat WHERE name ilike '%${name.trim()}%' AND is_delete = false`;

    pool.query(queryAll, (error, result) => {
      pool.query(query, (error2, result2) => {
        if (error2) {
          return response.send(400, {
            success: false,
            data: error2,
          });
        } else {
          return response.send(200, {
            success: true,
            data: result2.rows,
          });
        }
      });
    });
  });

  app.post("/api/addobat", (request, response) => {
    const { name, category, composition, segmentation, price, image } = request.body;

    const duplicateCheckQuery = `SELECT COUNT(*) as count FROM obat 
                                    WHERE name ilike '${name.trim()}' and is_delete = false`;

    pool.query(duplicateCheckQuery, (error, result) => {
      if (error) {
        return response.send(500, {
          success: false,
          error: "An error occured",
        });
      }

      const duplicateCount = Number(result.rows[0].count);
      if (duplicateCount > 0) {
        return response.send(200, {
          success: false,
          data: "Data Obat sudah terdaftar.",
        });
      }

      const query = `INSERT INTO obat (name, category, composition, segmentation, price, image, created_by, created_on)
                                    VALUES ('${name.trim()}', '${category.trim()}', '${composition.trim()}', '${segmentation.trim()}', ${price}, '${image}', '1', now());`;

      pool.query(query, (error, result) => {
        if (error) {
          return response.send(400, {
            success: false,
            data: error,
          });
        } else {
          return response.send(200, {
            success: true,
            data: `Data Obat ${name} saved`,
          });
        }
      });
    });
  });

  app.put("/api/updateobat/:id", (request, response) => {
    const { name, category, composition, segmentation, price, image } = request.body;
    const { id } = request.params;

    const duplicateCheckQuery = `SELECT COUNT(*) as count FROM obat
                                  WHERE (name ilike '${name.trim()}' AND id != ${id}) and is_delete = false`;

    pool.query(duplicateCheckQuery, (error, result) => {
      if (error) {
        return response.send(500, {
          success: false,
          error: "An error occured",
        });
      }

      const duplicateCount = Number(result.rows[0].count);
      if (duplicateCount > 0) {
        return response.send(200, {
          success: false,
          data: "Data obat sudah terdaftar",
        });
      }

      const query = `UPDATE obat
                      SET name = '${name.trim()}',
                          category = '${category.trim()}', 
                          composition = '${composition.trim()}', 
                          segmentation = '${segmentation.trim()}', 
                          price = ${price},
                          image = '${image}',
                          modified_by = 1,
                          modified_on = now()
                      WHERE id = ${id};`;

      pool.query(query, (error, result) => {
        if (error) {
          return response.send(400, {
            success: false,
            data: error,
          });
        } else {
          return response.send(200, {
            success: true,
            data: `Obat ${name} Updated`,
          });
        }
      });
    });
  });

  app.put("/api/deleteobat/:id", (request, response) => {
    const { id } = request.params;

    const query = `UPDATE obat
                    SET is_delete = true, deleted_by = 1, deleted_on = now()
                    WHERE id = ${id};`;

    pool.query(query, (error, result) => {
      if (error) {
        return response.send(400, {
          success: false,
          data: error,
        });
      } else {
        return response.send(200, {
          success: true,
          data: "Data has been deleted",
        });
      }
    });
  });
};
