import Modal from "react-bootstrap/esm/Modal"
import Button from "react-bootstrap/esm/Button"
import Form from "react-bootstrap/esm/Form"

export const PaymentModal = (props) => {
    const {show, close, grandTotal, handleChangePayment, payMoney, orderPayments, modePayment} = props
    
    var title
    var payMoneyinput

    if(modePayment === 'berhasil') {
        title = <Modal.Title>Pembayaran: Terima kasih!</Modal.Title>
        payMoneyinput = 
            <Form.Control 
                type="number" 
                placeholder="PayMoney"
                disabled
                onChange={(event) => handleChangePayment(event)}
            />
    } else {
        title = <Modal.Title>Pembayaran</Modal.Title>
        payMoneyinput = 
            <Form.Control 
                type="number" 
                placeholder="PayMoney"
                onChange={(event) => handleChangePayment(event)}
            />
    }
    
    return (
    <Modal show={show} onHide={close}>
        <Modal.Header closeButton>
            <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group className="mb-3">
                    <Form.Label>Total Bayar</Form.Label>
                    <Form.Control type="text" disabled value={grandTotal()} />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Nominal Pembayaran</Form.Label>
                    {payMoneyinput}
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Nominal Kembalian</Form.Label>
                    <Form.Control type="number" value={payMoney - grandTotal()} disabled />
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={close}>
                Batal
            </Button>
            {modePayment === "berhasil" ? (
                <></>
            ) : (
                <Button variant="success" onClick={orderPayments}>
                    Bayar!
                </Button>
            )}
        </Modal.Footer>
    </Modal>
    )
}