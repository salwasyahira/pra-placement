const express = require("express")
const dotenv = require("dotenv")
dotenv.config()
const bodyParser = require("body-parser")
const cors = require("cors")

const nodemailer = require("nodemailer")

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

global.config = require('./config/dbconfig')


app.get("/", (req, res) => {
  res.send("Hello, World");
});

require("./services/obatService")(app, global.config.pool);

app.listen(process.env.PORT, () => {
    console.log(`Server Listening on ${process.env.PORT}`);
  });