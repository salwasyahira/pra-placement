CREATE TABLE public.obat (
	id bigint primary key generated always as identity not null,
	name varchar(50),
	category varchar(50),
	composition text,
	segmentation varchar(50),
	price bigint,
	image varchar,
	created_by bigint not null,
	created_on timestamp not null,
	modified_by bigint,
	modified_on timestamp,
	deleted_by bigint,
	deleted_on timestamp,
	is_delete boolean default false not null
)

INSERT INTO public.obat ("name",category,composition,segmentation,price,image,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete) VALUES
	 ('Decolgen 4 Tablet','Flu','Paracetamol 400 mg, Phenylpropanolamine HCl 12.5 mg, Chlorpheniramine maleate 1 mg','Obat Bebas',2000,'https://d2qjkwm11akmwu.cloudfront.net/products/467867_7-2-2020_16-22-18-1665760963.webp',1,'2023-12-18 10:27:36.816814',1,'2023-12-19 11:00:23.304007',1,'2023-12-19 10:49:28.243935',false),
	 ('Promag 10 Tablet','Maag','Hydrotalcite 200 mg, Mg(OH)2 150 mg, Simethicone 50 mg','Obat Bebas (Hijau)',10000,'https://d2qjkwm11akmwu.cloudfront.net/products/881948_2-12-2021_16-26-6-1665834167.png',1,'2023-12-19 10:06:48.304494',1,'2023-12-21 10:29:19.112233',NULL,NULL,false),
	 ('Panadol Biru','Sakit Kepala','Paracetamol 500 mg','Obat Bebas',12500,'https://d2qjkwm11akmwu.cloudfront.net/products/222822_24-8-2022_11-38-12-1665760972.webp',1,'2023-12-18 14:06:01.420347',1,'2023-12-21 10:31:42.647759',1,'2023-12-19 10:07:00.959214',false),
	 ('Panadol Extra','Sakit kepala berat','Paracetamol 500 mg dan Caffeine 65 mg','Obat Bebas (Hijau)',14500,'https://d2qjkwm11akmwu.cloudfront.net/products/393357_22-8-2022_16-1-47-1665778679.webp',1,'2023-12-18 14:42:15.130727',1,'2023-12-21 10:32:06.612638',1,'2023-12-19 15:05:16.201867',false),
	 ('OBH Batuk 60 ml','Flu dan batuk','Succus Liquiritiae 167 mg, Paracetamol 150 mg, dan bahan lain','Obat Bebas (Biru)',18000,'https://d2qjkwm11akmwu.cloudfront.net/products/48241_19-1-2023_10-55-35.webp',1,'2023-12-20 10:32:00.025064',1,'2023-12-21 10:33:14.103525',NULL,NULL,false),
	 ('Paramex Flu & Batuk 4 Tablet','Flu dan batuk','Paracetamol 500 mg, Pseudoephedrine HCl 30 mg, dan Dextromethorphan HBr 15 mg','Obat Bebas (Biru)',2500,'https://d2qjkwm11akmwu.cloudfront.net/products/421685_13-7-2022_11-34-10-1665851421.webp',1,'2023-12-19 11:01:28.958871',1,'2023-12-21 10:35:00.524554',NULL,NULL,false),
	 ('Tolak Angin 5 Sachet','Masuk angin','Herbal dan madu','Herbal',22000,'https://api.watsons.co.id/medias/SM-TOLAK-ANGIN-CAIR-5S-14250.jpg?context=bWFzdGVyfHd0Y2lkL2ltYWdlc3w2MTIyNjd8aW1hZ2UvanBlZ3xhRGxtTDJoaU5pOHhNVFkyT0RNNU5UZzFNVGd3Tmk5VFRTQlVUMHhCU3lCQlRrZEpUaUJEUVVsU0lEVlRMVEUwTWpVd0xtcHdad3w0MjIyNzA3NGZjOWJjMGQzNmYxMzEwMDZlMGRiNDNmYzJmZmE3NGMyYTNkYzgxYjlmYzUyMDdiNGZmN2E3NTgx',1,'2023-12-20 10:29:19.52381',1,'2023-12-21 10:34:18.463794',1,'2023-12-21 10:37:38.388574',true),
	 ('Tolak Angin 5 Sachet','Masuk angin','Herbal dan madu','Herbal',22000,'https://api.watsons.co.id/medias/SM-TOLAK-ANGIN-CAIR-5S-14250.jpg?context=bWFzdGVyfHd0Y2lkL2ltYWdlc3w2MTIyNjd8aW1hZ2UvanBlZ3xhRGxtTDJoaU5pOHhNVFkyT0RNNU5UZzFNVGd3Tmk5VFRTQlVUMHhCU3lCQlRrZEpUaUJEUVVsU0lEVlRMVEUwTWpVd0xtcHdad3w0MjIyNzA3NGZjOWJjMGQzNmYxMzEwMDZlMGRiNDNmYzJmZmE3NGMyYTNkYzgxYjlmYzUyMDdiNGZmN2E3NTgx',1,'2023-12-21 10:38:24.464997',NULL,NULL,NULL,NULL,false);
