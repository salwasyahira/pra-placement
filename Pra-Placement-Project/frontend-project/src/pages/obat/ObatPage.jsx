import React from "react";
import obatService from "../../services/ObatService";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FormInputObat from "./ForminputObat";
import * as Icon from "react-bootstrap-icons";

export default class ObatPage extends React.Component {
  ObatModel = {
    name: "",
    category: "",
    composition: "",
    segmentation: "",
    price: "",
    image: "",
  };

  Params = {
    name: "",
  };

  constructor() {
    super();

    this.state = {
      listObat: [],
      show: false,
      ObatModel: this.ObatModel,
      errors: {},
      mode: "",
      params: this.Params,
    };
  }

  componentDidMount() {
    this.loadDataObat();
  }

  loadDataObat = async () => {
    const { params } = this.state;
    const response = await obatService.searchObat(params);

    if (response.success) {
      this.setState({
        listObat: response.data.data,
      });
    }
  };

  openModal = () => {
    this.setState({
      show: true,
      ObatModel: this.ObatModel,
      errors: {},
      mode: "create",
    });
  };

  closeModal = () => {
    this.setState({
      show: false,
    });
  };

  handleChange = (event) => {
    const { name, value } = event.target;
    // console.log(event);

    this.setState({
      ObatModel: {
        ...this.state.ObatModel,
        [name]: value,
      },
      errors: "",
    });
  };

  saveHandler = async () => {
    const { ObatModel } = this.state;

    if (this.validationHandler()) {
      const response = await obatService.addObat(ObatModel);

      if (response.success) {
        // jika validationHandler nya true maka masuk ke kondisi ini
        this.loadDataObat();
        this.setState({
          show: false,
          ObatModel: this.ObatModel,
        });
      } else {
        alert(response.data);
        // console.log(response.data)
      }
    }
  };

  validationHandler = () => {
    // untuk validasi supaya kalo name kosong maka tidak akan ke save
    const { ObatModel } = this.state;

    var formIsValid = true;
    var errors = {};
    // var check = /^[a-zA-Z]+[\sa-zA-Z]*$/.test(ObatModel.name);

    // if (ObatModel.name === "") {
    //   formIsValid = false;
    //   errors.name = "Nama tidak boleh kosong";
    // } else if (check !== true) {
    //   formIsValid = false;
    //   errors.name = "Nama obat salah, harap isi dengan benar";
    // }

    if (ObatModel.name === "") {
      formIsValid = false;
      errors.name = "Nama Obat tidak boleh kosong";
    }
    if (ObatModel.category === "") {
      formIsValid = false;
      errors.category = "Kategori Obat tidak boleh kosong";
    }
    if (ObatModel.segmentation === "") {
      formIsValid = false;
      errors.segmentation = "Segmentasi Obat tidak boleh kosong";
    }
    if (ObatModel.composition === "") {
      formIsValid = false;
      errors.composition = "Komposisi Obat tidak boleh kosong";
    }
    if (ObatModel.price === "") {
      formIsValid = false;
      errors.price = "Harga Obat tidak boleh kosong";
    }
    if (ObatModel.image === "") {
      formIsValid = false;
      errors.image = "Gambar Obat tidak boleh kosong";
    }

    this.setState({
      errors: errors,
    });
    return formIsValid;
  };

  openModalEdit = async (id) => {
    const response = await obatService.getObatById(id);

    if (response.success) {
      this.setState({
        show: true,
        ObatModel: response.data,
        mode: "edit",
        errors: "",
      });
    }
  };

  editHandler = async () => {
    const { ObatModel } = this.state;

    if (this.validationHandler()) {
      const response = await obatService.editObat(ObatModel);

      if (response.success) {
        this.loadDataObat();
        this.setState({
          show: false,
          ObatModel: this.ObatModel,
        });
      } else {
        alert(response.data);
        console.log("response", response);
      }
    }
  };

  openModalDelete = async (id) => {
    const response = await obatService.getObatById(id);

    if (response.success) {
      this.setState({
        show: true,
        ObatModel: response.data,
        mode: "delete",
      });
    }
  };

  deleteHandler = async () => {
    const { ObatModel } = this.state;

    const response = await obatService.deleteObat(ObatModel);

    if (response.success) {
      this.loadDataObat();
      this.setState({
        show: false,
        ObatModel: this.ObatModel,
      });
    }
  };

  handleChangeSearch = (event) => {
    console.log("event", event);
    const { value } = event.target;
    this.setState(
      {
        params: {
          ...this.state.params,
          name: value,
        },
      },
      () => this.loadDataObat()
    );
  };

  render() {
    const { listObat, show, ObatModel, errors, mode } = this.state;
    console.log("list Obat", listObat);
    console.log("obat model", ObatModel);

    return (
      <>
        <div>
          <h1
            class="card-title mb-0"
            style={{ marginLeft: "95px", marginTop: "40px" }}
          >
            Obat Page
          </h1>
          <div class="row">
            <div class="col">
              <div class="card flex-fill">
                <div class="card-header">
                  <Row style={{ marginTop: "20px" }}>
                    <Col lg={4}>
                      <Form.Control
                        style={{ width: "60%", marginLeft: "100px" }}
                        placeholder="Search by Nama Obat"
                        type="search"
                        onChange={this.handleChangeSearch}
                      />
                    </Col>

                    <Col>
                      <div>
                        <Button
                          variant="primary"
                          style={{
                            display: "flex",
                            justifyContent: "flex-end",
                            marginLeft: "810px",
                          }}
                          onClick={this.openModal}
                        >
                          Tambah
                        </Button>
                      </div>
                    </Col>
                  </Row>

                  <h1> </h1>

                  <div class="container text-center">
                    <table
                      class="table table-bordered"
                      style={{ textAlign: "center" }}
                    >
                      <thead>
                        <tr>
                          <th>Nama Obat</th>
                          <th>Kategori</th>
                          <th>Komposisi</th>
                          <th>Segmentasi</th>
                          <th>Harga</th>
                          <th>Gambar Obat</th>
                          <th>#</th>
                        </tr>
                      </thead>

                      <tbody>
                        {listObat &&
                          listObat.map((data, index) => {
                            return (
                              <tr key={index}>
                                <td>{data.name}</td>
                                <td>{data.category}</td>
                                <td>{data.composition}</td>
                                <td>{data.segmentation}</td>
                                <td>Rp{data.price}</td>
                                <td>
                                  <img
                                    src={data.image}
                                    alt="..."
                                    style={{ width: "180px", height: "180px" }}
                                  />
                                </td>
                                <td>
                                  <Button
                                    variant="warning"
                                    onClick={() => this.openModalEdit(data.id)}
                                  >
                                    <Icon.PencilSquare />
                                  </Button>
                                  <Button
                                    variant="danger"
                                    onClick={() =>
                                      this.openModalDelete(data.id)
                                    }
                                  >
                                    <Icon.Trash3 />
                                  </Button>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <FormInputObat
          show={show}
          close={this.closeModal}
          ObatModel={ObatModel}
          handleChange={this.handleChange}
          saveHandler={this.saveHandler}
          errors={errors}
          mode={mode}
          editHandler={this.editHandler}
          deleteHandler={this.deleteHandler}
        />
      </>
    );
  }
}
