import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

const FormInputObat = (props) => {
  const {
    show,
    close,
    ObatModel,
    handleChange,
    saveHandler,
    errors,
    mode,
    editHandler,
    deleteHandler,
  } = props;

  var title;
  var button;

  if (mode === "create") {
    title = <Modal.Title>Tambah Obat</Modal.Title>;
    button = (
      <Button variant="primary" onClick={saveHandler}>
        Simpan
      </Button>
    );
  } else if (mode === "edit") {
    title = <Modal.Title>Ubah Obat</Modal.Title>;
    button = (
      <Button variant="primary" onClick={editHandler}>
        Simpan
      </Button>
    );
  } else {
    title = <Modal.Title>Hapus Obat</Modal.Title>;
    button = (
      <Button variant="primary" onClick={deleteHandler}>
        Ya
      </Button>
    );
  }

  return (
    <>
      <Modal show={show} onHide={close}>
        <Modal.Header closeButton>{title}</Modal.Header>

        <Modal.Body>
          {mode === "delete" ? (
            <h5>Anda akan menghapus Obat {ObatModel.name}?</h5>
          ) : (
            <Form>
              <Form.Group className="mb-3">
                <Form.Label>Nama</Form.Label>
                <Form.Control
                  type="text"
                  onChange={handleChange}
                  name="name"
                  value={ObatModel.name}
                />
                <Form.Text className="text-danger">{errors.name}</Form.Text>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Kategori</Form.Label>
                <Form.Control
                  type="text"
                  onChange={handleChange}
                  name="category"
                  value={ObatModel.category}
                />
                <Form.Text className="text-danger">{errors.category}</Form.Text>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Komposisi</Form.Label>
                <Form.Control
                  type="text"
                  onChange={handleChange}
                  name="composition"
                  value={ObatModel.composition}
                />
                <Form.Text className="text-danger">
                  {errors.composition}
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Segmentasi</Form.Label>
                <Form.Control
                  type="text"
                  onChange={handleChange}
                  name="segmentation"
                  value={ObatModel.segmentation}
                />
                <Form.Text className="text-danger">
                  {errors.segmentation}
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Harga</Form.Label>
                <Form.Control
                  type="number"
                  onChange={handleChange}
                  name="price"
                  value={ObatModel.price}
                />
                <Form.Text className="text-danger">{errors.price}</Form.Text>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Masukkan URL Obat</Form.Label>
                <Form.Control
                  type="text"
                  onChange={handleChange}
                  name="image"
                  value={ObatModel.image}
                />
                <Form.Text className="text-danger">
                  {errors.segmentation}
                </Form.Text>
              </Form.Group>
            </Form>
          )}
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={close}>
            Batal
          </Button>
          {button}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default FormInputObat;
