var input = require("readline-sync");

function no1() {
  var uangAndi = input.questionInt("Masukkan uang Andi: ");
  var hargaKacamata = [500, 600, 700, 800];
  var hargaBaju = [200, 400, 350];
  var hargaSepatu = [400, 350, 200, 300];
  var hargaBuku = [100, 50, 150];

  var kacamataTerpilih = 0;
  var bajuTerpilih = 0;
  var sepatuTerpilih = 0;
  var bukuTerpilih = 0;
  var uangTerpakai = 0;
  var jumlahItem = 0;

  for (var i = 0; i < hargaKacamata.length; i++) {
    if (uangAndi >= hargaKacamata[i]) {
      if (hargaKacamata[i] <= kacamataTerpilih || kacamataTerpilih === 0) {
        kacamataTerpilih = hargaKacamata[i];
      }
    } // 500
  }
  if (uangAndi >= kacamataTerpilih && kacamataTerpilih !== 0) {
    uangAndi = uangAndi - kacamataTerpilih;
    jumlahItem += 1;
    uangTerpakai += kacamataTerpilih;
    // console.log("cek jml item kacamata: ", jumlahItem);
    // console.log("Masuk?");
  }

  for (var i = 0; i < hargaBaju.length; i++) {
    if (uangAndi >= hargaBaju[i]) {
      if (hargaBaju[i] <= bajuTerpilih || bajuTerpilih === 0) {
        bajuTerpilih = hargaBaju[i];
      }
    }
  }
  if (uangAndi >= bajuTerpilih && bajuTerpilih !== 0) {
    uangAndi = uangAndi - bajuTerpilih;
    jumlahItem += 1;
    uangTerpakai += bajuTerpilih;
    // console.log("cek jml item baju: ", jumlahItem);
  }
  for (var i = 0; i < hargaSepatu.length; i++) {
    if (uangAndi >= hargaSepatu[i]) {
      if (hargaSepatu[i] < sepatuTerpilih || sepatuTerpilih === 0) {
        sepatuTerpilih = hargaSepatu[i];
      }
    }
  }
  if (uangAndi >= sepatuTerpilih && sepatuTerpilih !== 0) {
    uangAndi = uangAndi - sepatuTerpilih;
    jumlahItem += 1;
    uangTerpakai += sepatuTerpilih;
    // console.log("cek jml item sepatu: ", jumlahItem);
  }
  for (var i = 0; i < hargaBuku.length; i++) {
    if (uangAndi >= hargaBuku[i]) {
      if (uangAndi == hargaBuku[i] || bukuTerpilih === 0) {
        bukuTerpilih = hargaBuku[i];
        break;
      } else if (hargaBuku[i] < bukuTerpilih) {
        bukuTerpilih = hargaBuku[i];
      }
    }
  }
  if (uangAndi >= bukuTerpilih && bukuTerpilih !== 0) {
    uangAndi = uangAndi - bukuTerpilih;
    jumlahItem += 1;
    uangTerpakai += bukuTerpilih;
    // console.log("cek jml item buku: ", jumlahItem);
  }

  console.log("Jumlah uang yang dipakai: ", uangTerpakai);
  console.log("Sisa uang Andi: ", uangAndi);
  console.log(
    `Jumlah item yg bisa dibeli:  ${jumlahItem} (kacamata: ${kacamataTerpilih}, baju: ${bajuTerpilih}, sepatu: ${sepatuTerpilih}, buku: ${bukuTerpilih})`
  );
}

function no2() {
  // denda perpustakaan

  var buku = ["A", "B", "C", "D"];
  var durasiPinjam = [14, 3, 7, 7];
  var dendaTelat = 100; // 100 per hari

  console.log("a. 28 Februari 2016 - 7 Maret 2016*"); // 2016 kabisat
  var hari1 = 8;
  var kasus1 = 0;
  var dendaBuku1 = [];
  for (var i = 0; i < durasiPinjam.length; i++) {
    if (durasiPinjam[i] < hari1) {
      kasus1 = Number((hari1 - durasiPinjam[i]) * dendaTelat);
      dendaBuku1.push(kasus1);
    }
    console.log(`Denda buku ${buku[i]}: Rp${kasus1}`);
  }

  console.log("b.	29 April 2018 - 30 Mei 2018");
  var hari2 = 31;
  var kasus2 = 0;
  var dendaBuku2 = [];
  for (var i = 0; i < durasiPinjam.length; i++) {
    if (durasiPinjam[i] < hari2) {
      kasus2 = Number((hari2 - durasiPinjam[i]) * dendaTelat);
      dendaBuku2.push(kasus2);
    }
    console.log(`Denda buku ${buku[i]}: Rp${kasus2}`);
  }
}

function no3() {
  // tarif parkir
  // 27 Januari 2019 05:00:01 |	27 Januari 2019 17:45:03
  // 27 Januari 2019 07:03:59	| 27 Januari 2019 15:30:06
  // 27 Januari 2019 07:05:00	| 28 Januari 2019 00:20:21
  // 27 Januari 2019 11:14:23	| 27 Januari 2019 13:20:00


  var masuk = input.question("Masuk : ");
  var keluar = input.question("Keluar: ");
  var tanggalMasuk = Number(masuk.substring(0, 2));
  var tanggalKeluar = Number(keluar.substring(0, 2));
  var waktuMasuk = masuk.substring(masuk.length - 8);
  var waktuKeluar = keluar.substring(keluar.length - 8);
  var jamMasuk = Number(waktuMasuk.substring(0, 2));
  var jamKeluar = Number(waktuKeluar.substring(0, 2));
  var hari = 0;
  var jam = 0;

  hari = tanggalKeluar - tanggalMasuk;
  jam = jamKeluar - jamMasuk;

  if (hari > 0) {
    jam = jam + 24;
  }

  if (jam <= 8) {
    tarif = jam * 1000;
  } else if (jam > 8 && jam <= 24) {
    tarif = 8000;
  } else if (jam > 24) {
    tarif = 15000 + (jam - 24) * 1000;
  }

  console.log(tarif);
}

function no4() {
  // fungsi untuk menampilkan n bilangan prima pertama

  var n = input.question("n = ");
  var prima = "";
  var total = 0;

  for (var i = 1; i <= n; i++) {
    total = 0;
    for (var j = 1; j < n; j++) {
      if (i % j == 0) {
        total += 1;
      }
    }
    if (total == 2) {
      prima += i + ",";
    } else {
      n++;
    }
  }
  prima = prima.substring(0, prima.length - 1);
  console.log(prima);
}
// no4()

function no5() {
  // fibonaci 1,1,2,3,5,8,13

  var n = input.question("Input n = ");
  var a = 1;
  var b = 0;
  var c = 0;
  var str = "";

  for (var i = 1; i <= n; i++) {
    if (i < n) {
      c = a + b;
      a = b;
      b = c;
      str = str + b + ",";
    } else {
      c = a + b;
      a = b;
      b = c;
      str = str + b;
    }
  }
  console.log(str);
}

function no6() {
  // palindrome

  var palindrome = input.question("Masukkan kalimat = ");
  var nampung = "";
  var hasil = "";

  for (var i = 0; i < palindrome.length; i++) {
    nampung = palindrome.substring(
      palindrome.length - 1 - i,
      palindrome.length - i
    );
    hasil += nampung;
  }

  if (hasil == palindrome) {
    console.log("Palindrome");
  } else {
    console.log("Not Palindrome");
  }
}
// no6()

function no7() {
  // mean, median, modus 8 7 0 2 7 1 7 6 3 0 7 1 3 4 6 1 6 4 3

  var angka = input.question("Masukkan deret angka = ");
  angka = angka.split(" ");
  angka = angka.sort(function (a, b) {
    return a - b;
  });
  console.log(angka);

  var sum = 0;
  var mean = 0;
  var median = 0;
  var nilaiTengah = Math.ceil(angka.length / 2);
  var modus = 0;

  // hitung mean dan modus
  for (var i = 0; i < angka.length; i++) {
    sum = sum + Number(angka[i]);
    if (angka[i] == angka[i + 1]) {
      jumlah += 1;
      modus = angka[i];
    } else {
      jumlah = 1;
    }
  }

  mean = sum / angka.length;

  // hitung median
  if (angka.length % 2 != 0) {
    median = Number(angka[nilaiTengah]);
  } else {
    median = (Number(angka[nilaiTengah - 1]) + Number(angka[nilaiTengah])) / 2;
  }

  console.log(
    "Mean = " + mean + ", Median = " + median + ", dan Modus = " + modus
  );
}

function no8() {
  var deret = input.question("Masukkan deret = ");
  deret = deret.split(" ");
  deret = deret.sort(function (a, b) {
    return a - b;
  });
  var max = 0;
  var min = 0;

  console.log(deret);

  for (var i = 0; i < 4; i++) {
    min = min + Number(deret[i]);
  }

  for (var i = deret.length - 1; i > deret.length - 5; i--) {
    max = max + Number(deret[i]);
  }
  console.log(min, max);
}

function no9() {
  var n = input.question("N = ");
  var sum = 0;
  var result = "";

  for (var i = 0; i < n; i++) {
    sum = sum + Number(n);
    result = result + sum + " ";
  }
  console.log(result);
}

function no10() {
  var kalimat = input.question("Input Kalimat = "); //susilo bambang
  var kata = kalimat.split(" "); //susilo, bambang
  var word = "";
  var kata1 = [];

  for (var i = 0; i < kata.length; i++) {
    kata1 = kata[i].split(""); //s,u,s,i,l,o

    for (var j = 0; j < kata1.length; j++) {
      if (j == 0) {
        word = word + kata1[j];
      } else if (j == kata1.length - 1) {
        word = word + kata1[j];
      } else {
        word = word + "*";
      }
    }
    if (i == kata.length - 1) {
      word = word + "";
    } else {
      word = word + " ";
    }
  }
  console.log(word);
}

function no11() {
  // outputnya:
  // **k**
  // **u**
  // **r**
  // **e**
  // **J**

  var kata = input.question("Masukka kata = ");
  var string = "";
  var n = Math.floor(kata.length / 2);

  for (var i = kata.length; i >= 0; i--) {
    for (var j = 0; j < n; j++) {
      string = string + "*";
    }
    string = string + kata.charAt(i);
    for (var j = 0; j < n; j++) {
      string = string + "*";
    }
    string = string + "\n";
  }
  console.log("Output: ", string);
}

function no12() {
  // sort without sort function
  // Input: 1 2 1 3 4 7 1 1 5 6 1 8

  var array = input.question("Input = ");
  array = array.split(" ");

  var sort = [];
  var angkaMin = array[0];
  var kecil = 0;
  // console.log(array);

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array.length; j++) {
      if (angkaMin > array[j]) {
        kecil = j;
        angkaMin = array[j];
      }
    }
    array.splice(kecil, 1);
    console.log(kecil)
    array.length += 1; // karena arraynya ke splice
    sort.push(angkaMin);

    angkaMin = array[0];
    kecil = 0;
  }
  console.log("Output: ", sort.toString().replace(/,/g, " "));
}

function no13() {
  // derajat sudut terkecil yang dibentuk 2 jarum jam

  var jam = input.questionInt('Masukkan Jam: ')
  var menit = input.questionInt('Masukkan Menit: ')

  if (jam < 0 || jam > 12 || menit < 0 || menit >= 60){
    console.log("Input salah, silahkan periksa kembali")
  } else {

    // Hitung sudut
    var sudut = Math.abs(30 * jam - (11 / 2) * menit); // 30*12 = 360 - 5.5 * 5 = 360 - 27.5 = 332.5

    // Pastikan sudut tidak lebih dari 180 derajat
    // Math.min(sudut, 360 - sudut)

    if (sudut > 180) {
      sudut = 360 - sudut
    }
  }
  console.log("Sudut: ", sudut)

}
no13()

function no14() {
  var deret = input.question("Deret = ");
  var n = input.question("n = ");
  var deretSplit = deret.split(" "); //ubah ke array

  for (var i = 0; i < n; i++) {
    deretSplit.push(deretSplit[0]);
    deretSplit.shift();
  }

  console.log(deretSplit.toString().replace(/,/g, " "));
}

function no15() {
  var time = input.question("Masukkan waktu = ");
  var amPM = time.substring(9, 11);
  var hour = time.substring(0, 2);
  var minuteSecod = time.substring(3, 8);
  var answer = 0;

  if (hour == 12 && amPM == "AM") {
    hour = "00";
  } else if (amPM == "AM") {
    hour = hour;
  } else if (hour == 12 && amPM == "PM") {
    hour = 12;
  } else if (amPM == "PM") {
    hour = Number(hour) + 12;
  }

  answer = hour + ":" + minuteSecod;
  console.log(answer);
}

function no16() {
  // Kamu dan 3 temanmu makan-makan di restoran
  // ada 1 orang yang alergi ikan
  // Pajak 10% dari harga makanan dan service 5% dari harga makanan.
  // berapa yang harus dibayar oleh masing-masing (1 orang bayar lebih murah karena alergi).

  var makanan = [
    "1. Tuna Sandwich (mengandung ikan): 42k",
    "2. Spaghetti Carbonara: 50k",
    "3. Tea pitcher: 30k",
    "4. Pizza: 70k",
    "5. Salad: 30k",
  ];
  var jumlahOrang = input.question("Jumlah orang yang makan = ");
  var jumlahAlergi = input.question("Jumlah orang yang alergi ikan = ");
  console.log("Menu makanan: ", makanan);
  var indeksAlergi = input.question("Masukkan indeks menu alergi = ");
  var harga = [42000, 50000, 30000, 70000, 30000];
  var pajak = 0.1;
  var service = 0.05;
  var totalHargaIkan = 0;
  var totalTanpaIkan = 0;

  for (var i = 0; i < harga.length; i++) {
    if (i != indeksAlergi - 1) {
      totalTanpaIkan += harga[i];
    } else {
      totalHargaIkan += harga[i];
    }
  }
  console.log(totalTanpaIkan);

  var hargaAlergi = Math.round(
    (totalTanpaIkan + totalTanpaIkan * pajak + totalTanpaIkan * service) /
      jumlahOrang
  );

  var hargaTidakAlergi = Math.round(
    (totalHargaIkan + totalHargaIkan * pajak + totalHargaIkan * service) /
      (jumlahOrang - jumlahAlergi) +
      hargaAlergi
  );
  console.log(hargaAlergi);
  console.log(hargaTidakAlergi);
}

function no17() {
  // hattori N N T N N N T T T T T N T T T N T N

  var rute = input.question("Rute Hattori = ");
  rute = rute.split(" ");
  var total = 0;
  var naik = 1;
  var turun = 1;
  var gunung = 0;
  var lembah = 0;
  var str = "";

  for (var i = 0; i < rute.length; i++) {
    if (rute[i] == "N") {
      total += naik;
      if (i == rute.length - 1) {
        str += total;
      } else {
        str += total + " ";
      }
    } else {
      total -= turun;
      if (i == rute.length - 1) {
        str += total;
      } else {
        str += total + " ";
      }
    }
  }

  var hattori = str.split(" ");
  console.log(hattori);
  for (var i = 0; i < hattori.length; i++) {
    if (Number(hattori[i - 1]) > 0 && Number(hattori[i]) == 0) {
      gunung += 1;
    } else if (Number(hattori[i - 1]) < 0 && Number(hattori[i]) == 0) {
      lembah += 1;
    }
  }

  console.log("Gunung = " + gunung + " dan Lembah = " + lembah);
}

function no18() {
  // Jika Donna mulai olahraga jam 18,
  // berapa cc air yang akan diminum Donna sepanjang berolahraga?

  var jam = [9, 13, 15, 17];
  var kalori = [30, 20, 50, 80];
  var mulaiOlahraga = 18;
  var selisihWaktu = 0;
  var menitOlahraga = 0;
  var minum = 0;

  for (var i = 0; i < jam.length; i++) {
    selisihWaktu = mulaiOlahraga - jam[i];
    menitOlahraga = menitOlahraga + 0.1 * kalori[i] * selisihWaktu;
  }
  minum = Math.floor(menitOlahraga / 30) * 100 + 500;
  console.log(
    "Banyak air yang diminum Donna sepanjang olahraga: ",
    minum,
    "cc"
  );
}

function no19() {
  // pangram

  var pangram = input.question("Masukkan kalimat = ");
  pangram = pangram.toLowerCase();
  pangram = pangram.replace(/ /g, "");
  pangram = pangram.split("");
  pangram = pangram.sort();
  // console.log(pangram)

  var result = [];
  var hasil = "";

  for (var i = 0; i < pangram.length; i++) {
    if (pangram[i] != pangram[i + 1]) {
      result = result + pangram[i];
    }
  }

  if (result.length == 26) {
    hasil = "Pangram";
  } else {
    hasil = "Not Pangram";
  }

  console.log(hasil);
}

function no20() {
  // var A = ["G", "G", "G"];
  // var B = ["B", "B", "B"];
  var jarakAwal = input.questionInt("Masukkan jarak awal: ")
  var AJalan = 0;
  var BJalan = jarakAwal;

  var lanjutMain = "y"
  while (lanjutMain === "y"){

    console.log("INPUT HURUF G (GUNTING), K (KERTAS), B (BATU)")
    var A = input.question("Suit A: ")
    var B = input.question("Suit B: ")


    for (var i = 0; i < A.length; i++) {
      if (A[i] === "G" && B[i] === "K") {
        AJalan += 2; 
        BJalan += 1;
      } else if (A[i] === "G" && B[i] === "B") {
        AJalan -= 1;
        BJalan -= 2;
      } else if (A[i] === "K" && B[i] === "B") {
        AJalan += 2;
        BJalan += 1;
      } else if (A[i] === "K" && B[i] === "G") {
        AJalan -= 1;
        BJalan -= 2;
      } else if (A[i] === "B" && B[i] === "G") {
        AJalan += 2;
        BJalan += 1;
      } else if (A[i] === "B" && B[i] === "K") {
        AJalan -= 1;
        BJalan -= 2;
      }
    }
    console.log("Jalan A saat ini: ", AJalan)
    console.log("Jalan B saat ini: ", BJalan)
    if (AJalan === BJalan){
      if (AJalan < 0 && BJalan <0){
        console.log("B Menang")
        break;
      } else {
        console.log("A Menang")
        break;
      }
    }
    lanjutMain = input.question("Lanjut main? (y / n)")
  }
}

// no1();
// no2()
// no3()
// no4();
// no5();
// no6()
// no7()
// no8()
// no9()
// no10()
// no11()
// no12()
// no13()
// no14()
// no15()
// no16()
// no17()
// no18()
// no19()
// no20()
