import axios from "axios";
import { config } from "../config/Config";

const obatService = {
  getAllObat: () => {
    const result = axios
      .get(config.apiurl + "api/obat")
      .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },

  addObat: (data) => {
    const result = axios
      .post(config.apiurl + "api/addobat", data)
      .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },

  getObatById: (id) => {
    const result = axios
      .get(config.apiurl + `api/obat/${id}`)
      .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },

  editObat: (data) => {
    const result = axios
      .put(config.apiurl + `api/updateobat/${data.id}`, data)
      .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },

  deleteObat: (data) => {
    const result = axios
      .put(config.apiurl + `api/deleteobat/${data.id}`, data)
      .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },

  searchObat: (data) => {
    const result = axios
    .get(config.apiurl + `api/search/obat?name=${data.name}`)
    .then((response) => {
      return {
        success: response.data.success,
        data: response.data
      }
    })
    .catch((error) => {
      return {
        success: false,
        data: error,
      }
    })
    return result
  }
};

export default obatService;
