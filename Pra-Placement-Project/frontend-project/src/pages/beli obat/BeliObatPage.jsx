import React from "react";
import obatService from "../../services/ObatService";
import { Card, Col, Row, Button, Container } from "react-bootstrap";
import { PaymentModal } from "./PaymentModal";

export default class BeliObatPage extends React.Component {
  constructor() {
    super();

    this.state = {
      listObat: [],
      listKeranjang: [],
      show: false,
      payMoney: 0,
    };
  }

  componentDidMount() {
    this.loadDataObat();
  }

  loadDataObat = async () => {
    const response = await obatService.getAllObat();

    if (response.success) {
      this.setState({
        listObat: response.data,
      });
    }
  };

  handleTambahKeranjang = (data, index) => {
    var { listObat } = this.state;

    var insertQty = { ...data, quantity: 1, product: 1 };
    listObat[index] = insertQty;

    this.setState({
      listObat: listObat,
      // listKeranjang: [...updateData, insertQty],
    });
  };

  handleTambahQuantity = (index) => {
    const { listObat } = this.state;
    const quantitys = listObat[index].quantity;
    const addQuantity = quantitys + 1;

    var updateData = [...this.state.listObat];
    updateData[index] = { ...updateData[index], quantity: addQuantity };

    this.setState({
      listObat: updateData,
    });

    // console.log("index", quantitys);
  };

  handleKurangQuantity = (index) => {
    const { listObat } = this.state;
    const quantitys = listObat[index].quantity;
    const products = listObat[index].product;

    const addQuantity = quantitys - 1;

    if (addQuantity == 0) {
      var addProducts = 0;
    } else if (quantitys > 0) {
      addProducts = products;
    }

    var updateData = [...this.state.listObat];
    updateData[index] = {
      ...updateData[index],
      quantity: addQuantity,
      product: addProducts,
    };

    this.setState({
      listObat: updateData,
    });
  };

  handleTambahProduct = (item) => {
    // var { listObat } = this.state;

    if (item.product == undefined) {
      item.product = 0;
    }

    return Number(item.product);
  };

  grandTotalProduct = () => {
    var { listObat } = this.state;

    var grandTotalProduct = listObat.reduce(
      (total, item) => total + Number(this.handleTambahProduct(item)),
      0
    );

    return grandTotalProduct;
  };

  calculateTotal = (item) => {
    // console.log("q", item.quantity);
    // console.log(item.price_max);

    if (item.quantity == undefined) {
      item.quantity = 0;
    }
    return Number(item.price) * Number(item.quantity);
  };

  grandTotal = () => {
    var { listObat } = this.state;

    var grandTotal = listObat.reduce(
      (total, item) => total + Number(this.calculateTotal(item)),
      0
    );

    return grandTotal;
  };

  openModalBayar = () => {
    this.setState({
      show: true,
    });
  };

  closeModalBayar = () => {
    this.setState({
      show: false,
    });
  };

  handleChangePayment = (event) => {
    // console.log(event)

    this.setState({
      payMoney: Number(event.target.value),
    });
  };

  orderPayments = async () => {
    const { payMoney } = this.state;

    if (payMoney === 0) {
      alert("Masukkan uang Anda");
    } else if (payMoney < this.grandTotal()) {
      alert("Uang yang Anda masukkan kurang");
    } else {
      this.setState({
        modePayment: "berhasil",
      });
    }
  };

  render() {
    const { listObat, show, payMoney, modePayment } = this.state;
    console.log("paymoney", payMoney);

    var footer;

    if (this.grandTotalProduct() != 0) {
      footer = (
        <>
          <nav
            class="navbar sticky-bottom bg-body-tertiary"
            style={{ height: "80px" }}
          >
            <Container>
              <a class="navbar-brand" href="#">
                {this.grandTotalProduct()} Produk | Total Harga : Rp{" "}
                {this.grandTotal()}
              </a>
              <Button onClick={this.openModalBayar}>
                <i class="bi bi-cart2"></i> Bayar
              </Button>
            </Container>
          </nav>
        </>
      );
    } else {
      footer = <></>;
    }
    return (
      <>
        <div>
          <div class="container-fluid p-0">
            <Row>
              <Col sm={1}>
                {/* <img
                  src="https://i.pinimg.com/originals/bf/12/c0/bf12c05be7f73150f05115653979c510.png"
                  alt="Obat"
                  class="img-fluid rounded mb-2"
                  style={{marginTop: "10px", marginLeft: "50px", width: "200px", height: "80px"}}
                /> */}
              </Col>

              <Col>
                <h1
                  class="card-title mb-0"
                  style={{ marginLeft: "10px", marginTop: "25px" }}
                >
                  Beli Obat
                </h1>
              </Col>
            </Row>
          </div>

          <div class="row">
            <div class="col">
              <div class="card flex-fill">
                <div class="card-header">
                  <Row style={{ marginTop: "20px" }}>
                    {listObat &&
                      listObat.map((data, index) => (
                        <Col sm={6}>
                          <Card style={{marginBottom: "20px"}}>
                            <Card.Body>
                              <Row style={{ height: "100px" }}>
                                <Col>
                                  <Card.Title>{data.name}</Card.Title>
                                  <Card.Text
                                    style={{
                                      fontSize: "12px",
                                      marginTop: "5px",
                                      marginLeft: "12px",
                                    }}
                                  >
                                    Rp {data.price}
                                  </Card.Text>
                                  <Card.Text
                                    style={{
                                      fontSize: "16px",
                                      marginTop: "-8px",
                                    }}
                                  >
                                    {data.composition}
                                  </Card.Text>
                                </Col>
                                <Col sm={3}>
                                  <img
                                    src={data.image}
                                    alt="..."
                                    style={{ width: "120px", height: "120px" }}
                                  />
                                </Col>
                              </Row>
                              <Row
                                style={{
                                  marginTop: "12px",
                                  textAlign: "center",
                                }}
                              >
                                {/* {listObat.map((data2, index) => ( */}
                                <>
                                  {data.quantity ? (
                                    <>
                                      <Col
                                        style={{
                                          justifyContent: "center",
                                          marginLeft: "100px",
                                        }}
                                      >
                                        <Button
                                          style={{ color: "navy" }}
                                          variant="light"
                                          onClick={() =>
                                            this.handleKurangQuantity(index)
                                          }
                                        >
                                          {" "}
                                          -<i class="bi bi-dash"></i>{" "}
                                        </Button>
                                      </Col>
                                      <Col sm={1}>
                                        <Card.Text style={{ marginTop: "6px" }}>
                                          {data.quantity}
                                        </Card.Text>
                                      </Col>
                                      <Col>
                                        <Button
                                          variant="light"
                                          style={{
                                            color: "navy",
                                            marginRight: "120px",
                                          }}
                                          onClick={() =>
                                            this.handleTambahQuantity(index)
                                          }
                                        >
                                          {" "}
                                          +<i class="bi bi-plus"></i>
                                        </Button>
                                      </Col>
                                    </>
                                  ) : (
                                    <Col style={{ justifyContent: "center" }}>
                                      <Button
                                        style={{ width: "360px" }}
                                        onClick={() =>
                                          this.handleTambahKeranjang(
                                            data,
                                            index
                                          )
                                        }
                                      >
                                        Tambah ke Keranjang
                                      </Button>
                                    </Col>
                                  )}
                                </>
                                {/* ))} */}

                                <Col style={{ textAlign: "center" }} sm={3}>
                                  {/* <Button
                                  variant="outline-primary"
                                  style={{ width: "80px" }}
                                >
                                  Detail
                                </Button> */}
                                </Col>
                              </Row>
                            </Card.Body>
                          </Card>
                        </Col>
                      ))}
                  </Row>
                </div>
              </div>
            </div>
          </div>
        </div>
        {footer}

        <PaymentModal
          show={show}
          close={this.closeModalBayar}
          grandTotal={this.grandTotal}
          handleChangePayment={this.handleChangePayment}
          payMoney={payMoney}
          modePayment={modePayment}
          orderPayments={this.orderPayments}
        />
      </>
    );
  }
}
