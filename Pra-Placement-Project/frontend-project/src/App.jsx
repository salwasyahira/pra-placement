import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import ObatPage from "./pages/obat/ObatPage";
import BeliObatPage from "./pages/beli obat/BeliObatPage";

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <>
        <BrowserRouter>
          <Routes>
            <Route path="/obat" element={<ObatPage />} />
            <Route path="/beliobat" element={<BeliObatPage />} />

          </Routes>
        </BrowserRouter>
      </>
    );
  }
}
